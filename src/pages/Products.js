import React from 'react'
import AddItem from '../components/AddItem'
import AllItem from '../components/AllItem'

function Inventory() {
  return (
    <>
    <div className='inventory d-flex'>
        <AddItem /> 
    </div>
    <AllItem />
    </>
    
  )
}

export default Inventory