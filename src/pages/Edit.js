import React, { useState } from 'react'
import { Button, Form} from 'react-bootstrap'
import { useParams } from 'react-router-dom';

function Edit() {
    const [ pQty, setPqty] = useState('')
    const {id} = useParams()

    const EditItem = (event) => {
        event.preventDefault()
        
        fetch(`https://cryptic-thicket-66196.herokuapp.com/pokemon/order/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                qty: pQty
            })
        }).then(res => res.json()).then(data => {
            if (data) {
                window.location.href = '/inventory'
            } else {
                alert('invalid')
            }
        })
    }
    
    return (
        <Form className='edit-form' onSubmit={e => EditItem(e)}>
            <Form.Group>
                <Form.Label> Quantity:</Form.Label>
                <Form.Control type='number' value={pQty} onChange={e => {setPqty(e.target.value)}} required></Form.Control>
            </Form.Group> 
            <br />
            <Button type="submit" className="btn edit-btn btn-primary" > Edit </Button>
        </Form>
     
    );
}

export default Edit