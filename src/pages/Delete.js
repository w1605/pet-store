import { useParams, Navigate } from 'react-router-dom';

export default function DeleteItem() {
	const {id} = useParams()

	fetch(`https://cryptic-thicket-66196.herokuapp.com/pokemon/order/${id}`, {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		if (data) {
			return true
		} else {
			return false
		}
	})

    return(
        <Navigate to='/inventory' />
    )
}

