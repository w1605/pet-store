import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import Home from "./pages/Home";
import Inventory from "./pages/Products";
import AboutUs from "./pages/AboutUs";
import Delete from "./pages/Delete";
import Edit from "./pages/Edit";

function App() {
  return (
    <>
      <Router>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/inventory" element={<Inventory />} />
          <Route path="/aboutus" element={<AboutUs />} />
          <Route path="/inventory/delete/:id" element={<Delete />} />
          <Route path="/inventory/edit/:id" element={<Edit />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
