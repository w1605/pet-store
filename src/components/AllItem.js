import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Edit from "../pages/Edit";

function AllItem() {
  const [inventoryCollection, setInventoryCollection] = useState([]);
  useEffect(() => {
    fetch("https://cryptic-thicket-66196.herokuapp.com/pokemon/orders")
      .then((res) => res.json())
      .then((data) => {
        setInventoryCollection(
          data.map((inventory) => {
            return inventory;
          })
        );
      });
  }, [inventoryCollection, setInventoryCollection]);

  return (
    <div className="table">
      <table>
        <tr>
          <th>Pet Name</th>
          <th>Types</th>
          <th>Abilities</th>
          <th>Qty</th>
          <th>Action</th>
        </tr>
        {inventoryCollection.map((val, key) => {
          return (
            <tr key={key}>
              <td>{val.petName}</td>
              <td>{val.types.join(", ")}</td>
              <td>{val.abilities.join(", ")}</td>
              <td>{val.qty}</td>
              <td>
                <Link className="btn btn-primary" to={`delete/${val._id}`}>
                  Delete
                </Link>
                <Link className="btn btn-primary" to={`edit/${val._id}`}>
                  Edit
                </Link>
              </td>
            </tr>
          );
        })}
      </table>
    </div>
  );
}

export default AllItem;
