import React, { useState } from "react";
import { Button, Offcanvas, Form } from "react-bootstrap";

function AddItem() {
  const [show, setShow] = useState(false);
  const [petName, setPetName] = useState("");
  const [qty, setQty] = useState("");
  const [types, setTypes] = useState("-");
  const [abilities, setAbilities] = useState("-");
  const [dropdown, setDropdown] = useState([]);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const AddItemSubmit = (event) => {
    event.preventDefault();

    fetch(
      `https://cryptic-thicket-66196.herokuapp.com/pokemon/add/${petName}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          qty: qty,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          window.location.href = "/inventory";
        } else {
          alert("Pokemon not found");
        }
      });
  };

  // console.log(dropdown);

  const PokemonDetailsTypes = () => {
    fetch(`https://pokeapi.co/api/v2/pokemon/${petName}`)
      .then((res) => res.json())
      .then((data) => {
        let types = data.types;
        let type = types.map((typeName) => {
          return typeName.type.name;
        });
        setTypes(type.join(", "));
      });
  };

  const PokemonDetailsAbilities = () => {
    fetch(`https://pokeapi.co/api/v2/pokemon/${petName}`)
      .then((res) => res.json())
      .then((data) => {
        let abilities = data.abilities;
        let ability = abilities.map((abilityName) => {
          return abilityName.ability.name;
        });
        setAbilities(ability.join(", "));
      });
  };

  fetch("https://pokeapi.co/api/v2/pokemon/?offset=0&limit=100")
    .then((res) => res.json())
    .then((data) => {
      let array = data.results;
      setDropdown(
        array.map((pName) => {
          return pName;
        })
      );
    });

  const handleSelect = (e) => {
    setPetName(e.target.value);
  };

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Add Item
      </Button>

      <Offcanvas show={show} onHide={handleClose}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Add an Item</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <Form onSubmit={(e) => AddItemSubmit(e)}>
            <Form.Group>
              <Form.Label> Pet Name:</Form.Label>
              <Form.Select
                onChange={handleSelect}
                aria-label="Default select example"
              >
                <option value={petName}>--Select--</option>
                {dropdown.map((val, key) => (
                  <option key={val.name} value={val.name}>
                    {val.name}
                  </option>
                ))}
              </Form.Select>
            </Form.Group>
            <br />

            <Form.Group onChange={PokemonDetailsTypes()}>
              <Form.Label> Type/s</Form.Label>
              <br />
              <Form.Text value={types}>{types}</Form.Text>
            </Form.Group>
            <br />

            <Form.Group onChange={PokemonDetailsAbilities()}>
              <Form.Label> Abilities </Form.Label>
              <br />
              <Form.Text value={abilities}>{abilities}</Form.Text>
            </Form.Group>
            <br />

            <Form.Group>
              <Form.Label> Quantity:</Form.Label>
              <Form.Control
                type="number"
                value={qty}
                onChange={(e) => {
                  setQty(e.target.value);
                }}
                required
              ></Form.Control>
            </Form.Group>
            <br />
            <Button type="submit" className="btn btn-primary">
              Submit
            </Button>
          </Form>
        </Offcanvas.Body>
      </Offcanvas>
    </>
  );
}

export default AddItem;
